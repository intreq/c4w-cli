package nl.practicom.c4w.cli

import nl.practicom.c4w.multidll.dto.ProcedureInfo
import nl.practicom.c4w.multidll.io.MultiFileTxaProcedureWriter
import nl.practicom.c4w.multidll.io.SingleProcedureTxaWriter
import nl.practicom.c4w.multidll.transforms.procedure.ConvertToExternalProcedure
import nl.practicom.c4w.multidll.transforms.procedure.ConvertToPrivateProcedure
import nl.practicom.c4w.multidll.transforms.procedure.ConvertToPublicProcedure
import nl.practicom.c4w.multidll.transforms.procedure.ProcedureExtractor
import nl.practicom.c4w.multidll.transforms.procedure.ProcedureIdentityTransform
import nl.practicom.c4w.multidll.transforms.procedure.ProcedureListReader
import nl.practicom.c4w.multidll.transforms.procedure.ProcedureListTransformFactory
import nl.practicom.c4w.multidll.transforms.procedure.ProcedureTransform
import nl.practicom.c4w.multidll.transforms.procedure.ProcedureTransformChain
import nl.practicom.c4w.multidll.transforms.procedure.ProcedureTransformFactory
import nl.practicom.c4w.multidll.io.ProcedureWriter
import nl.practicom.c4w.multidll.io.SingleTxaProcedureWriter
import nl.practicom.c4w.txa.transform.StreamingTxaReader
import picocli.CommandLine

import java.nio.file.InvalidPathException
import java.nio.file.Path
import java.util.concurrent.Callable

@CommandLine.Command(name = "ExtractProceduresCommand", mixinStandardHelpOptions = true, version = "1.0")
class ExtractProceduresCommand implements Callable<Integer> {

    @CommandLine.Option(names = ["-s","--source-txa"], description =  "Path to source TXA", required = true)
    Path sourceTxaPath

    @CommandLine.Option(names = ["-o","--output"], description =  "Path to destination TXA or folder", required = true)
    Path destination

    @CommandLine.Option(names = ["-m","--module-size"], description =  "Number of procedures per module", required = false, defaultValue = "20")
    int moduleSize

    @CommandLine.Option(names = ["-n","--num-splits"], description =  "Number of parts to split into", required =false, defaultValue = "1")
    int numSplits

    @CommandLine.Option(names = ["-f","--filter-file"], description = "File with procedure names to include", required = false)
    Path filterFilePath

    @Override
    Integer call() throws Exception {

        def procedureExtractor

        if (filterFilePath) {
            procedureExtractor = buildFilteredProcedureExtractor()
        } else {
            procedureExtractor = buildUnfilteredProcedureExtractor()
        }

        try {
            new StreamingTxaReader()
                .withHandler(procedureExtractor)
                .parse(sourceTxaPath.toFile())

            return 0
        } catch (Exception e){
            return 1
        }
    }

    ProcedureExtractor buildUnfilteredProcedureExtractor(){
        def writer
        def procedureTransform = new ProcedureTransformChain()

        if ( moduleSize == 1){
            writer = new SingleProcedureTxaWriter(destination)
        } else {
            if (numSplits <= 1) {
                writer = new SingleTxaProcedureWriter(destination, moduleSize)
            } else {
                writer = createMultiFileWriter()
                // In this case the writer doubles as a transform to
                // replace the procedure category by the output file name
                procedureTransform.append(writer as ProcedureTransform)
            }
        }
        procedureTransform.append(new ProcedureIdentityTransform())

        return new ProcedureExtractor(
                {ProcedureInfo pi -> procedureTransform} as ProcedureTransformFactory,
                writer)
    }

    ProcedureExtractor buildFilteredProcedureExtractor() {
        def transformFactory = buildProcedureVisibilityTransformFactory()

        def writer

        def publicTransform = new ProcedureTransformChain()
                .append(new ConvertToPublicProcedure())

        def privateTransform = new ProcedureTransformChain()
                .append(new ConvertToPrivateProcedure())

        def externalTransform = new ProcedureTransformChain()
                .append(new ConvertToExternalProcedure())

        if ( moduleSize == 1){
            writer = new SingleProcedureTxaWriter(destination)
        } else {
            if (numSplits <= 1) {
                writer = new SingleTxaProcedureWriter(destination, moduleSize)
            } else {
                writer = createMultiFileWriter()
                publicTransform.append(writer as ProcedureTransform)
                privateTransform.append(writer as ProcedureTransform)
                externalTransform.append(writer as ProcedureTransform)
            }
        }

        transformFactory.publicTransform = publicTransform.append(new ProcedureIdentityTransform())
        transformFactory.privateTransform = privateTransform.append(new ProcedureIdentityTransform())
        transformFactory.externalTransform = externalTransform.append(new ProcedureIdentityTransform())

        return new ProcedureExtractor(transformFactory, writer)
    }


    ProcedureListTransformFactory buildProcedureVisibilityTransformFactory() {
        def filterFile = filterFilePath.toFile()
        if (!filterFile.exists()) {
            throw new Exception("Filter file not found: ${filterFilePath}")
        }
        if (!filterFile.isFile()) {
            throw new Exception("Specified filter file is not a file: ${filterFilePath}")
        }
        def listReader = new ProcedureListReader()
        listReader.read(filterFile)

        def transformFactory = new ProcedureListTransformFactory()
        transformFactory.publicProcedures = listReader.publicProcedures
        transformFactory.privateProcedures = listReader.privateProcedures
        transformFactory.externalProcedures = listReader.externalProcedures
        return transformFactory
    }

    ProcedureWriter createMultiFileWriter() {
        if (!destination.toFile().exists()) {
            if (!destination.toFile().mkdirs()) {
                throw new IllegalAccessException("Unable to create destination folder ${destination}")
            }
        }

        if (!destination.toFile().isDirectory()) {
            throw new InvalidPathException(
                    destination.toAbsolutePath().toString(),
                    "Destination for splitting a txa should be a folder",
                    -1)
        } else {
            def prefix = sourceTxaPath.getFileName().toString()
            if ( prefix.indexOf('.') > 0){
                prefix = prefix.substring(0, prefix.indexOf('.') )
            }
            return new MultiFileTxaProcedureWriter(destination, prefix, numSplits, moduleSize)
        }
    }

    static void main(String... args){
        System.exit(new CommandLine(new ExtractProceduresCommand()).execute(args));
    }
}
