package nl.practicom.c4w.cli

import nl.practicom.c4w.multidll.transforms.application.ApplicationType
import nl.practicom.c4w.multidll.transforms.application.TxaApplicationTransform
import nl.practicom.c4w.multidll.transforms.application.TxaApplicationTransformOptions
import nl.practicom.c4w.txa.transform.StreamingTxaReader
import picocli.CommandLine

import java.nio.file.Path
import java.util.concurrent.Callable

@CommandLine.Command(name = "CloneApplicationCommand", mixinStandardHelpOptions = true, version = "1.0")
class CloneApplicationCommand implements Callable<Integer> {

    @CommandLine.Option(names = ["-n","--application-name"], description =  "Application name")
    String applicationName

    @CommandLine.Option(names = ["-t","--application-type"], description = "Type of application")
    ApplicationType applicationType

    @CommandLine.Option(names = ["-o","--output-folder"], description = "Absolute path to the output folder")
    Path destinationFolder

    @CommandLine.Parameters(index = "0", description = "Absolute path to the source txa file")
    Path sourceTxaPath

    @Override
    Integer call() throws Exception {
        File targetTxaFile = destinationFolder.resolve(applicationName + ".txa").toFile()
        File sourceTxaFile = sourceTxaPath.toFile()

        TxaApplicationTransformOptions transformOptions = new TxaApplicationTransformOptions(
                applicationName: this.applicationName,
                targetType: this.applicationType
        )

        def txaTransform = new TxaApplicationTransform(targetTxaFile, transformOptions)
        def txaReader = new StreamingTxaReader()
        txaReader.registerHandler(txaTransform)
        txaReader.parse(sourceTxaFile)
        return 0
    }

    static void main(String... args){
        System.exit(new CommandLine(new CloneApplicationCommand()).execute(args));
    }
}
