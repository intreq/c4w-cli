package nl.practicom.c4w.cli

import nl.practicom.c4w.multidll.transforms.project.ClarionProjectTransformer
import nl.practicom.c4w.multidll.transforms.project.ProjectTransformOptions
import nl.practicom.c4w.multidll.transforms.application.ApplicationType
import picocli.CommandLine

import java.nio.file.Path
import java.util.concurrent.Callable

@CommandLine.Command(name = "CloneProjectCommand", mixinStandardHelpOptions = true, version = "1.0")
class CloneProjectCommand implements Callable<Integer> {
    @CommandLine.Option(names = ["-n","--application-name"], description =  "Application name")
    String applicationName

    @CommandLine.Option(names = ["-t","--application-type"], description = "Type of application")
    ApplicationType applicationType

    @CommandLine.Option(names = ["-o","--output-folder"], description = "Absolute path to the output folder")
    Path destinationFolder

    @CommandLine.Parameters(index = "0", description = "Absolute path to the source txa file")
    Path sourceProjectPath

    @Override
    Integer call() throws Exception {
        File sourceProjectFile = sourceProjectPath.toFile()
        File targetProjectFile = destinationFolder.resolve(applicationName + ".cwproj").toFile()

        ProjectTransformOptions options = new ProjectTransformOptions(
                applicationType: this.applicationType,
                assemblyName: this.applicationName,
                outputName: this.applicationName
        )

        def transform = new ClarionProjectTransformer(options)
        try {
            transform.convert(sourceProjectFile.newReader(), targetProjectFile.newWriter())
            return 0
        } catch(Exception x) {
            System.out.println(x.getMessage())
            return 1
        }
    }

    static void main(String... args){
        System.exit(new CommandLine(new CloneProjectCommand()).execute(args));
    }
}
