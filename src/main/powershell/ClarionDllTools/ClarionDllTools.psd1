@{
    RootModule = 'ClarionDllTools.psm1'
    ModuleVersion = '1.0.0'
    PowerShellVersion = '6.1'
    GUID = '63ff946a-8c58-419c-8bbd-5c0c6cc8bd7c'
    Author = 'Stefan Warringa (Intreq)'
    CompanyName = 'Practicom B.V.'
    Description = "Clarion for Windows Multi-DLL Tools"
    FunctionsToExport = @(
        'ConvertTo-DataDLLProject',
        'ConvertTo-DataDLLTxa',
        'ConvertTo-MainApplicationProject',
        'ConvertTo-MainApplicationTxa',
        'ConvertTo-ProcedureDLLProject',
        'ConvertTo-ProcedureDLLTxa',
        'Export-Procedures',
        'Get-ClarionToolsConfiguration',
        'Initialize-MultiDLLWorkspace',
        'Set-MultiDLLWorkspace',
        'Show-ClarionToolsConfiguration',
        'Update-ClarionToolsConfiguration'
    )
   CmdletsToExport = @()
    VariablesToExport = '*'
    AliasesToExport = @()
    CompatiblePSEditions = 'Core'
    RequiredModules = @(
        @{
            ModuleName="ClarionBuildTools"
            ModuleVersion="1.0.0"
        }
    )
    PrivateData = @{
        PSData = @{
            ProjectSite = 'https://bitbucket.practicom.net/projects/CLARIONTOOLS/repos/c4w-cli/'
            ExternalModuleDependencies = @(
                'ClarionBuildTools'
            )            
        }
    }
}