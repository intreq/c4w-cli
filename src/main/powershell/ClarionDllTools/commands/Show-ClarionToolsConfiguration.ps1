function Show-ClarionToolsConfiguration {
  Get-ClarionToolsConfiguration | Format-List
}