<#
    .SYNOPSIS
    Sets up a standard folder structure for converting Clarion applications into multiple DLL's

    .DESCRIPTION
    To facilitate standardization and transferrability when working on decomposing a monolitihic
    Clarion application into multiple DLL's the multi-DLL scripts support the concept of a
    workspace with a standard folder structure. It also supports symbolic abbreviations that
    can be used in the script parameters. Eg. the '@work' symbol points to the workspace root.
    The workspace (@work) is set up with the following folders:

    source - contains the source application to be converted (@source)
    target - contains the new restructured application (@target)
    source_exports - contains the TXA files exported from the source application
    txa_converted - contains TXA files output by the conversion scripts
    cytoscape_exports - contains TXA files exported by the Cytoscape TXA analysis
    
    Other folders can be added by the user as needed but are not used by the scripts. 
    However they can be addressed using @work/foldername.
#>
Function Initialize-MultiDLLWorkspace {
    param(
        # Path to the folder containing the solution(s)/applications(s) to be converted
        [Parameter(Mandatory=$true)]
        [string] $Path,

        # Path to the workspace folder. Folder should not exists or is empty.
        [Parameter(Mandatory=$false)]
        [string] $Destination,

        # The application(s) or solution(s) to be converted
        [Parameter(Mandatory=$true)]
        [string[]] $Applications,

        # Copy application files (.sln, .cwproj and .app) to the target directory as well
        [Parameter(Mandatory=$False)]
        [switch]
        $CopyApplications,

        # Location of the clarion installation to use
        [Parameter(Mandatory=$false)]
        [string]
        $ClarionInstallation,

        # Copy the clarion installation to the workspace
        [Parameter(Mandatory=$false)]
        [switch]
        $CopyClarion
    )

    # Required for retrieving the solution items of a solution
    Import-Module -Name ClarionBuildTools

    if (-not (Test-Path $Path)){
        Throw "$Path not found"
    }

    # Destination defaults to current folder
	if ( $Destination ){
		$WorkspaceRoot = $Destination
	} else {
		$WorkspaceRoot = Get-Location
    }
    
    # If no clarion installation provided look for one below the specified path
    if (!$ClarionInstallation){
        # Look for Clarion installation inside Path
        $clarion = (Get-ChildItem -Path $Path -Include clarion.exe -Recurse)
        if ($clarion -ne $null){
            $ClarionInstallation = Split-Path (Split-Path $clarion)
        }
    }
	
	$SourceFolder = Join-Path -Path $WorkspaceRoot -ChildPath "source"
	$SourceExportsFolder = Join-Path -Path $WorkspaceRoot -ChildPath "source_exports"
	$ConvertedTxaFolder = Join-Path -Path $WorkspaceRoot -ChildPath "txa_converted"
	$CyExportsFolder = Join-Path -Path $WorkspaceRoot -ChildPath  "cytoscape_exports"
	$TargetFolder = Join-Path -Path $WorkspaceRoot -ChildPath  "target"
    $ClarionFolder = Join-Path -Path $WorkspaceRoot -ChildPath  "clarion"

    if (Test-Path $WorkspaceRoot){
        $directoryInfo = Get-ChildItem $WorkspaceRoot | Measure-Object		
		If ( $directoryInfo.count -gt 0 ) {
			Throw "Destination should point to a non-existent or empty folder!"
		}
    } else {
		$WorkspaceRoot = New-Item -ItemType Directory -Path $WorkspaceRoot
	}

    $filesToCopy = @("*.dct","*.ips","*.ico","*.jpg","*.png","*.gif","unicore.bat","imagecfg.exe","LibXl*","*.RED","*.ps1","*.RSC")
    $foldersToCopy = @("accessory")

    function Copy-File($file){
        Write-Host "Copying $(Split-Path -Path $file -Leaf)"
        Copy-Item -Path $file -Destination $SourceFolder

        $ext = Split-Path -Path $file -Extension

        if($ext -in @('.app','.sln','.cwproj') -and $CopyApplications){
            Copy-Item -Path $file -Destination $TargetFolder          
        }
    }

    try {
        Write-Host "Creating folder structure"
        $SourceFolder = New-Item -ItemType Directory -Path $SourceFolder
        $SourceExportsFolder = New-Item -ItemType Directory -Path $SourceExportsFolder
        $ConvertedTxaFolder = New-Item -ItemType Directory -Path $ConvertedTxaFolder
        $CyExportsFolder = New-Item -ItemType Directory -Path $CyExportsFolder
        $TargetFolder = New-Item -ItemType Directory -Path $TargetFolder

        Update-ClarionToolsConfiguration -Key WorkspaceLocation -Value $WorkspaceRoot
        Update-ClarionToolsConfiguration -Key SourceLocation -Value $SourceFolder
        Update-ClarionToolsConfiguration -Key TargetLocation $TargetFolder


        if ($ClarionInstallation -and (Test-Path -Path $ClarionInstallation)){
            Update-ClarionToolsConfiguration -Key ClarionInstallation -Value $ClarionInstallation
        }

        $Globs =  @($Applications)
        Write-Host "Processing applications $Globs"

        foreach ($glob in $Globs) {        
            $appFiles = Join-Path -Path $Path -ChildPath $glob -Resolve | Where-Object { (Split-Path -Path $_ -Extension) -eq '.app' }
            $slnFiles = Join-Path -Path $Path -ChildPath $glob -Resolve | Where-Object { (Split-Path -Path $_ -Extension) -eq '.sln' }

            foreach ($file in $appFiles){
                $appName = Split-Path -Path $file -LeafBase
                $projectFile = Join-Path -Path (Split-Path -Path $file) -ChildPath "$($appName).cwproj"

                Copy-File $file 
                Copy-File $projectFile
            }

            foreach ($sln in $slnFiles){
                Copy-File $sln

                $solutionItems = Get-ClarionSolutionItems -Path $sln
                foreach($sli in $solutionItems){
                    $appFile = Join-Path -Path $Path -ChildPath "$($sli).app"
                    $projectFile = Join-Path -Path $Path -ChildPath "$($sli).cwproj"

                    Copy-File $appFile
                    Copy-File $projectFile                  
                }
            }            
        }

        # Copy dct to source because clarion wants dct to be there when opening an app
        $dctFiles = Get-ChildItem -Path $Path | Where-Object { (Split-Path -Path $_ -Extension) -eq '.dct' }
        foreach ($dctFile in $dctFiles) {
            Copy-File $dctFile
        }

        # Copy resources required for build
        foreach ($f in $filesToCopy){
            Copy-Item -Path (Join-Path -Path $Path -ChildPath $f) -Destination $SourceFolder -ErrorAction SilentlyContinue
            Copy-Item -Path (Join-Path -Path $Path -ChildPath $f) -Destination $TargetFolder -ErrorAction SilentlyContinue
        }

        foreach ($f in $FoldersToCopy){
            Copy-Item -Path (Join-Path -Path $Path -ChildPath $f) -Recurse -Destination  $SourceFolder -ErrorAction SilentlyContinue
            Copy-Item -Path (Join-Path -Path $Path -ChildPath $f) -Recurse -Destination $TargetFolder -ErrorAction SilentlyContinue
        }

        if ($CopyClarion){
            if ($ClarionInstallation){
                New-Item -ItemType Directory -Path $ClarionFolder
                Copy-Item -Path "$($ClarionInstallation )\*" -Recurse -Destination $ClarionFolder
                Update-ClarionToolsConfiguration -Key ClarionInstallation -Value $ClarionFolder
            } else {
                Write-Error "Unable to copy clarion: no installation found"
            }
        }

        Show-ClarionToolsConfiguration

    } catch {
        Write-Host "Failure creating workspace: " $_.Exception.Message
        Remove-Item -Path $WorkspaceRoot -Recurse -Force
    }
}