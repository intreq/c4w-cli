Function Export-Procedures
{
  [CmdletBinding()]
  Param
  (
    # The source txa from which the application txa will be generated
    [Parameter(Position = 0, Mandatory = $false)]
    [string] $Path,

    # File or Folder the txa('s) should be written. Defaults to workspace
    [Parameter(Position = 0, Mandatory = $false)]
    [string] $Destination,

    # Number of procedures per module. Set to 0 if no modules should be used
    [Parameter(Mandatory = $false)]
    [int] $ModuleSize = 20,

    # The number of file-parts to use for splitting. Default is 1: single txa output
    [Parameter(Mandatory = $false)]
    [int] $NumberOfSplits = 1,

    # File containing the names of the procedures to include
    [Parameter(Mandatory = $false)]
    [string] $Filter,

    # Force overwrite of destination file(s)
    [switch] $Force
  )

  if ($Path)
  {
    $SourceTxaPath = $Path
  }
  else
  {
    $DefaultApp = Resolve-DefaultApplication
    if (!$DefaultApp)
    {
      Throw "No default application: specify the source txa file name via -Path"
    }
    else
    {
      $AppBasename = Split-Path -Path $DefaultApp -LeafBase
      $SourceTxaPath = "@work/source_exports/$AppBasename.txa"
    }
  }

  $SourceTxaPath = Resolve-Location -Path $SourceTxaPath -MustResolve

  If (!((Split-Path -Path $SourceTxaPath -Extension) -in ".txa",".apv"))
  {
    throw "$SourceTxaPath is not a .txa or .apv file"
  }

  # Check the combination of ModuleSize and NumberOfSplits to determine what to do
  If ($PSBoundParameters.ContainsKey('NumberOfSplits'))
  {
    If($NumberOfSplits -lt 2)
    {
      Die "NumberOfSplits should be greater than 1"
    }

    $MultipleTxaOutput = $true
  }
  Elseif ($PSBoundParameters.ContainsKey('ModuleSize')) 
  {
    # ModuleSize of 1 will generate seperate txa per procedure
    $MultipleTxaOutput = ($ModuleSize -eq 1)
  }
  Else 
  {
    # Use deafult settings: generate single txa with 20 procedures per module
    $ModuleSize = 20
    $MultipleTxaOutput = $false
  }

  # Resolve the destination
  If (!$PSBoundParameters.ContainsKey('Destination'))
  {
    $Destination = Resolve-Location -Path "@work/source_exports/"
  }
  Else 
  {
    $Destination = Resolve-Location -Path $Destination
    # $Destination may not exist!
    $Destination = $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath($Destination)
  }

  # Check if destination is compatible with the requested operation
  # For single output destination should be a filename, for multiple
  # txa's it should be a folder.
  # When it is create any missing folders in the path
  If (Test-Path -Path $Destination -ErrorAction SilentlyContinue)
  {
    If ((Get-Item -Path $Destination) -is [system.io.directoryinfo])
    {
      # Destination is a folder: only valid when generating multiple txa's
      If (!$MultipleTxaOutput)
      {
        Die "Destination should be a file"
      }
      Else 
      {
        # prevent accidental overwrites
        $DirInfo = Get-ChildItem $Destination| Measure-Object
        If(($DirInfo.Count -gt 0)  -and (!$Force))
        {
          Die "Folder $Destination is not empty. Use -Force to overwrite"
        }
      }
    }
    Else
    {
      # Destination set to a file: only valid when generating single txa
      If ($MultipleTxaOutput)
      {
        Die "Destination should be a folder"
      }
      Else
      {
        # prevent accidental overwrites
        If ((Test-Path -Path $Destination -ErrorAction SilentlyContinue) -and (!$Force))
        {
          Die "File $Destination already exists. Use -Force to overwrite"
        }
      }
    }
  }
  Else
  {
    # Destination does not exist. Create the parent folder structure
    If ($MultipleTxaOutput)
    {       
        If ( (Split-Path -Path $Destination -Extension) -eq '')
        {
          Write-Host "Creating $Destination"
          New-Item -ItemType Directory -Path $Destination -Force -ErrorAction SilentlyContinue
        }
        Else 
        {
          # There's an extension. We can technically create a folder with an extension
          # but that's probably not what the user wants
          Die "Destination should be a folder"
        }
    }
    Else 
    {
      $Parent = Split-Path -Path $Destination -Parent
      If (!(Test-Path $Parent))
      {
        Write-Host "Creating $Parent"
        New-Item -ItemType Directory -Path $Parent -Force -ErrorAction SilentlyContinue
      }
    }
  }

  Write-Host "Extracting procedures from $( $SourceTxaPath ) into $( $Destination )"

  If ($Filter)
  {
    $Filter = Resolve-Location -Path $Filter -MustResolve
  }

  [string[]]$CommandArgs = @(
  "--source-txa", $SourceTxaPath,
  "--module-size", $ModuleSize,
  "--num-splits", $NumberOfSplits,
  "--output", $Destination
  )

  if ($Filter)
  {
    $CommandArgs += "--filter-file"
    $CommandArgs += $Filter
  }

  Invoke-JavaCliCommand -CommandClass "nl.practicom.c4w.cli.ExtractProceduresCommand" -CommandArgs $CommandArgs
}