Function ConvertTo-MainApplicationTxa
{
  Param(
    # Name of the application. This will be used as the txa file name and inside the txa
    [Parameter(Mandatory = $false)]
    [string] $ApplicationName,

    # Folder where the txa file should be written
    [Parameter(Mandatory = $false)]
    [string] $Destination,
    
    # Force overwrite of existing file
    [Parameter(Mandatory = $false)]
    [switch] $Force,

    # The source txa from which the application txa will be generated
    [Parameter(Position = 0, Mandatory = $false)]
    [string] $Path
  )

  if ( $Path )
  {
    $SourceTxaPath = $Path
  }
  else
  {
    $DefaultApp = Resolve-DefaultApplication
    if (!$DefaultApp)
    {
      Throw "No default application: specify the source project name via -Path"
    }
    else
    {
      $SourceTxaPath = Split-Path -Path $DefaultApp -LeafBase
    }
  }

  if ( -not (Split-Path $SourceTxaPath -Extension) )
  {
    $SourceTxaPath = "$($SourceTxaPath).txa"
  }

  If ( (Split-Path -Path $SourceTxaPath -Leaf) -eq $SourceTxaPath )
  {
    $SourceTxaPath = "@work\source_exports\$($SourceTxaPath)"
  }

  $SourceTxaPath = Resolve-Location -Path $SourceTxaPath -MustResolve

  If (-not ($ApplicationName))
  {
    $ApplicationName = Split-Path -Path $SourceTxaPath -LeafBase
  }


  $OutputFolder = Resolve-Location -Path $Destination -DefaultPath "@work\txa_converted"
  

  New-ClarionTxa `
  -ApplicationType MainApplication `
  -ApplicationName $ApplicationName `
  -OutputFolder $OutputFolder `
  -Force:$Force `
  -SourceTxa $SourceTxaPath
}