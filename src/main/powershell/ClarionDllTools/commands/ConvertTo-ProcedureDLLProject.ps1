Function ConvertTo-ProcedureDLLProject
{
  Param(
    # Name of the application. This will be used as the cwproj file name
    # and inside the cwproj to set the output name and assembly name
    [Parameter(Mandatory = $false)]
    [string] $ApplicationName,

    # Folder where the cwproj file should be written
    [Parameter(Mandatory = $false)]
    [string] $Destination,
    
    # Force overwrite of existing file
    [Parameter(Mandatory = $false)]
    [switch] $Force,

    # The source cwproj from which the application cwproj will be generated
    [Parameter(Position = 0, Mandatory = $false)]
    [string] $Path
  )

  if ( $Path )
  {
    $SourceProjectPath = $Path
  }
  else
  {
    $DefaultApp = Resolve-DefaultApplication
    if (!$DefaultApp)
    {
      Throw "No default application: specify the source project name via -Path"
    }
    else
    {
      $SourceProjectPath = Split-Path -Path $DefaultApp -LeafBase
    }
  }

  if ( -not (Split-Path $SourceProjectPath -Extension) )
  {
    $SourceProjectPath = "$($SourceProjectPath).cwproj"
  }

  If ( (Split-Path -Path $SourceProjectPath -Leaf) -eq $SourceProjectPath )
  {
    $SourceProjectPath = "@source\$($SourceProjectPath)"
  }

  $SourceProjectPath = Resolve-Location -Path $SourceProjectPath -MustResolve

  If (-not ($ApplicationName))
  {
    $ApplicationName = Split-Path -Path $SourceProjectPath -LeafBase
    $ApplicationName ="${ApplicationName}_procedures"
  }


  $OutputFolder = Resolve-Location -Path $Destination -DefaultPath "@target"


  New-ClarionProjectFile `
  -ApplicationType ProcedureDLL `
  -ApplicationName $ApplicationName `
  -OutputFolder $OutputFolder `
  -Force:$Force `
  -SourceProjectFile $SourceProjectPath
}