﻿Function Set-MultiDLLWorkspace {
    param(
        [Parameter(Mandatory=$false)]
        [string] $Path
    )

    if ( $Path ) {
        $workspaceRoot = $Path
    } else {
        $workspaceRoot = Get-Location
    }

    $SourceFolder = Join-Path -Path $workspaceRoot -ChildPath "source"
    $TargetFolder = Join-Path -Path $workspaceRoot -ChildPath "target"

    Update-ClarionToolsConfiguration -Key WorkspaceLocation -Value $WorkspaceRoot
    Update-ClarionToolsConfiguration -Key SourceLocation -Value $SourceFolder
    Update-ClarionToolsConfiguration -Key TargetLocation $TargetFolder

    Show-ClarionToolsConfiguration

}