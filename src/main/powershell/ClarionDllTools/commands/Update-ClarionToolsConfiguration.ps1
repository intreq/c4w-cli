﻿Function Update-ClarionToolsConfiguration
{
    Param
    (
        [Parameter(Mandatory = $false)]
        [ValidateSet(
            'MinimalMsBuildVersion',
            'WorkspaceLocation',
            'SourceLocation',
            'TargetLocation',
            'ClarionInstallation'
        )]
        [string] $Key,

        [Parameter(Mandatory = $true)]
        [string] $Value
    )

    If ( $Key -in 'WorkspaceLocation','SourceLocation','TargetLocation','ClarionInstallation' )
    {
        If ( -not (Resolve-Path -Path $Value -ErrorAction SilentlyContinue) )
        {
            throw "Invalid path: $Value"
        }
        Else
        {
            $Value = (Resolve-Path -Path $Value)
        }
    }

    $ToolsConfig = Get-ClarionToolsConfiguration

    $ToolsConfig | Add-Member -MemberType NoteProperty -Name $Key -Value $Value -Force -Verbose

    $LocalAppData = [Environment]::GetFolderPath('ApplicationData')
    $ConfigFolder =  "$LocalAppData/ClarionTools"
    $ConfigFile = "$LocalAppData/ClarionTools/settings.json"

    if ( -not (Test-Path $ConfigFolder) )
    {
        New-Item $ConfigFolder -ItemType Directory -ErrorAction SilentlyContinue
    }

    $ToolsConfig | ConvertTo-Json -Depth 4 | Out-File $ConfigFile -Force
}