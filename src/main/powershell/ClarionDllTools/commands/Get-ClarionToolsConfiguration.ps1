Function Get-ClarionToolsConfiguration(){
  $LocalAppData = [Environment]::GetFolderPath('ApplicationData')
  $ConfigFile = "$LocalAppData/ClarionTools/settings.json"

  $ToolsConfig  = @{}

  If (Test-Path -Path $ConfigFile)
  {
    $ToolsConfig = (Get-Content -Raw -Path $ConfigFile | ConvertFrom-Json)
  }

  return $ToolsConfig
}