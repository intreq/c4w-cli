
$InternalFunctions = @(Get-ChildItem -Path $PSScriptRoot\internal\*.ps1 -ErrorAction SilentlyContinue)
$ModuleFunctions = @(Get-ChildItem -Path $PSScriptRoot\commands\*.ps1 -ErrorAction SilentlyContinue)
$ToExport = $ModuleFunctions | Where-Object { $_.BaseName -notin $InternalFunctions } | Select-Object -ExpandProperty BaseName

# Dot-source the files.
foreach ($import in $InternalFunctions) {
    try {
        Write-Verbose "Importing $($import.FullName)"
        . $import.FullName
    } catch {
        Write-Error "Failed to import function $($import.FullName): $_"
    }
}

foreach ($import in $ModuleFunctions) {
    try {
        Write-Verbose "Importing $($import.FullName)"
        . $import.FullName
    } catch {
        Write-Error "Failed to import function $($import.FullName): $_"
    }
}

Export-ModuleMember -Function $ToExport