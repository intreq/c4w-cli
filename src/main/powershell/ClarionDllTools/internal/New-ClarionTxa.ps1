Function New-ClarionTxa
{
  Param(
    # Application type
    [Parameter(Mandatory = $true)]
    [ValidateSet('MainApplication', 'ProcedureDLL', 'DataDLL')]
    [string] $ApplicationType,

    # Name of the application. This will be used as the txa file name and inside the txa
    [Parameter(Mandatory = $true)]
    [string] $ApplicationName,

    # Folder where the txa file should be written
    [Parameter(Mandatory = $true)]
    [string] $OutputFolder,
    
    # Force overwrite of existing file
    [Parameter(Mandatory = $false)]
    [switch] $Force,

    # The source txa from which the application txa will be generated
    [Parameter(Position = 0, Mandatory = $true)]
    [string] $SourceTxa
  )

  If(-not (Test-Path -Path $OutputFolder -ErrorAction SilentlyContinue))
  {
    New-Item -Path $OutputFolder -ItemType Directory -Force | Out-Null
  }

  If(-not $Force -and (Test-Path -Path (Join-Path -Path "$OutputFolder" -ChildPath "$ApplicationName.txa")))
  {
    Throw "File already exists: $ApplicationName.txa Use -Force to overwrite it"
  }

  [string[]]$CommandArgs = @(
  "--application-name", $ApplicationName,
  "--application-type", "$ApplicationType",
  "--output-folder", "$OutputFolder",
  $SourceTxa
  )

  Invoke-JavaCliCommand -CommandClass "nl.practicom.c4w.cli.CloneApplicationCommand" -CommandArgs $CommandArgs
}