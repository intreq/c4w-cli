function script:Die($Message) {
    [Console]::ForegroundColor = 'red'
    [Console]::Error.WriteLine($Message)
    [Console]::ResetColor()
    Exit-PSSession
    Throw $Message
}