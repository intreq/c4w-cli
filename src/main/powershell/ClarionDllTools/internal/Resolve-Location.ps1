Function Resolve-Location {
  Param
  (
    # The symbolic or actual path
    [Parameter(Position=0, Mandatory=$false)]
    [string] $Path,

    [Parameter(Mandatory=$false)]
    [string] $DefaultPath,

    [Parameter(Mandatory=$false)]
    [Switch] $MustResolve
  )

  $cfg = Get-ClarionToolsConfiguration

  If(-not $Path)
  {
    $Path = $DefaultPath
  }

  If ( $Path.ToUpper().StartsWith("@SOURCE") )
  {
    If($cfg.SourceLocation)
    {
      $Path = Join-Path -Path $cfg.SourceLocation -ChildPath $Path.Remove(0,8)
    }
    Else
    {
      Throw "Unable to resolve @Source. Please configure key 'SourceLocation'."
    }
  }
  elseif ( $Path.ToUpper().StartsWith("@WORK") )
  {
    If($cfg.WorkspaceLocation)
    {
      $Path = Join-Path -Path $cfg.WorkspaceLocation -ChildPath $Path.Remove(0,5)
    }
    Else
    {
      Throw "Unable to resolve @WORK. Please configure key 'WorkspaceLocation'."
    }
  } elseif  ( $Path.ToUpper().StartsWith("@TARGET") )
  {
    If($cfg.TargetLocation)
    {
      $Path = Join-Path -Path $cfg.TargetLocation -ChildPath $Path.Remove(0,7)
    }
    Else
    {
      Throw "Unable to resolve @TARGET. Please configure key 'TargetLocation'."
    }
  }

  If($MustResolve)
  {
    If (Resolve-Path -Path $Path -ErrorAction SilentlyContinue)
    {
      $Path = Resolve-Path -Path $Path
    }
    Else
    {
      throw "Invalid path: " + $Path
    }
  }

  return $Path
}