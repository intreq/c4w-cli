Function New-ClarionProjectFile
{
  Param(
    # Application type
    [Parameter(Mandatory = $true)]
    [ValidateSet('MainApplication', 'ProcedureDLL', 'DataDLL')]
    [string] $ApplicationType,

    # Name of the application. This will be used as the project and application  file name
    [Parameter(Mandatory = $true)]
    [string] $ApplicationName,

    # Folder where the project file should be written
    [Parameter(Mandatory = $true)]
    [string] $OutputFolder,

    # Force overwrite of existing file
    [Parameter(Mandatory = $false)]
    [switch] $Force,

    # The source project from which the converted project will be generated
    [Parameter(Position = 0, Mandatory = $true)]
    [string] $SourceProjectFile
  )


  If(-not (Test-Path -Path $OutputFolder -ErrorAction SilentlyContinue))
  {
    New-Item -Path $OutputFolder -ItemType Directory -Force | Out-Null
  }
  
  If(-not $Force -and (Test-Path -Path (Join-Path -Path "$OutputFolder" -ChildPath "$ApplicationName.cwproj")))
  {
    Throw "File already exists: $ApplicationName.cwproj Use -Force to overwrite it"
  }

  [string[]]$CommandArgs = @(
  "--application-name", $ApplicationName,
  "--application-type", "$ApplicationType",
  "--output-folder", "$OutputFolder",
  $SourceProjectFile
  )

  Invoke-JavaCliCommand -CommandClass "nl.practicom.c4w.cli.CloneProjectCommand" -CommandArgs $CommandArgs
}