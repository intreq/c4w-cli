Function Invoke-JavaCliCommand {
    Param
    (
        [string] $CommandClass,
        [string[]] $CommandArgs
    )

    $Java = Get-Command -Name "Java"
    $CliJar = Join-Path -Path $PSScriptRoot -ChildPath (Get-ChildItem -Path $PSScriptRoot -Name 'c4w-cli-*.jar')
    $ArgsString = ($CommandArgs -join " ")
    Start-Process -Wait -NoNewWindow -FilePath $Java.Source -ArgumentList "-cp", "`"$CliJar`"", $CommandClass, $ArgsString
}