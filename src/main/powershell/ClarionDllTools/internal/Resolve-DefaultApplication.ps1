Function Resolve-DefaultApplication
{
  $SourceFolder = Resolve-Location "@work/source"
  $AppFiles = Get-ChildItem -Path $SourceFolder | Resolve-Path | Where-Object { (Split-Path -Path $_ -Extension) -eq '.app' }
  If ($AppFiles.Count -eq 1)
  {
    $DefaultApp = $AppFiles[0] | Convert-Path  
    return $DefaultApp
  }
  Else
  {
    return $null
  }
}